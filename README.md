# Re-Mind

An application for optimizing learning, practice, and memorization. It is said that we learn exponentially: we need to be reminded often of new things we learn, but we can go longer between reminders of things we've known for a long time. This app tracks the things you want to learn and reminds you according to how long you've known them.

## Definitions

- activity - unique unit of knowledge. This could be a word, number, song, passage, or other idea you want to learn and retain knowledge of
  -  duration - time it takes to practice the activity. This may decrease through time as proficiency increases.
  -  interval - time between practicing the activity. 
    - The initial interval is based off of the original duration
    - upon successful practice (nailed it), each successive interval increases in proportion to the previous
    - upon decent practice, interval decreases by half
    - upon unsuccessful practice, the interval is reset to the activity duration
- practice - act of carrying out an activity
  - nailed it - activity is carried out up to your standard
  - decent - activity not is carried out completely, but not up to your standard
  - failed it - activity not is carried out up to your standard.
- session - time set aside to practice activitys. 
  - Often this is regular, periodic occurance of a set duration
  - though this program does not discriminate between planned and sponteneous sessions.
