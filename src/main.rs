use chrono::Utc;
use remind::*;
use std::io::stdin;
use std::vec::Vec;
use structopt::StructOpt;

use log::{debug, error, info};

#[derive(StructOpt)]
#[structopt(name = "ReMind", about = "An app for practice and memorization")]
struct Opt {
    /// Print example activities
    #[structopt(short, long)]
    example: bool,
    #[structopt(short, long)]
    list: bool,
    #[structopt(short, long)]
    add: Option<String>,
    #[structopt(short, long)]
    content: Option<String>,
    #[structopt(short, long)]
    value: Option<String>,
    #[structopt(short, long)]
    tags: Option<Vec<String>>,
    #[structopt(short, long, default_value = "./.regimen.json")]
    regimen_file: String,
}

fn main() {
    env_logger::init();
    let opt = Opt::from_args();

    if opt.example {
        print_example();
        return;
    }

    let tags = opt.tags.unwrap_or(vec![]);

    if opt.list {
        if let Some(regimen) = load_regimen(&opt.regimen_file) {
            for a in regimen.activities {
                if tags.is_empty() || a.tags.iter().any(|t| tags.contains(t)) {
                    println!("{}", a.title);
                    if !a.tags.is_empty() {
                        println!("  tags: {:?}", a.tags)
                    }
                }
            }
            return;
        }
    }

    if let Some(new_activity) = opt.add {
        if let Some(mut regimen) = load_regimen(&opt.regimen_file) {
            regimen.add_activity(&new_activity, opt.content, opt.value, &tags);
            if save_regimen(&regimen, &opt.regimen_file).is_some() {
                info!("Added '{}'", new_activity);
            } else {
                error!("Failed to save regimen");
            }
            return;
        }
        println!("Unable to open regimen");
        return;
    }

    if let Some(mut regimen) = load_regimen(&opt.regimen_file) {
        while let Some(activity) = regimen.pop_activity(Utc::now(), &tags) {
            println!("Practice '{}': [N]ailed it!, [D]ecent, [F]ailed it..., What is it [?]. Press Enter when finished", activity.title);
            if let Some(content) = activity.get_content() {
                println!("  Content: '{}'", content);
            }
            debug!("Before: {:#?}", activity);
            let user_entry = loop {
                let mut s = String::new();
                stdin().read_line(&mut s).unwrap();
                if s.trim() == "?" {
                    if let Some(value) = activity.get_value() {
                        println!("  Value: '{}'", value);
                    }
                } else {
                    break s;
                }
            };
            let r_value = match user_entry
                .trim()
                .to_lowercase()
                .chars()
                .nth(0)
                .unwrap_or('*')
            {
                'n' => std::f32::consts::E,
                'd' => 1.0 / std::f32::consts::E,
                'f' => 0.0,
                '*' => {
                    regimen.activities.push(activity);
                    break;
                }
                _ => 1.0,
            };
            regimen.push_activity(activity, r_value, Utc::now());
            save_regimen(&regimen, &opt.regimen_file);
        }
        println!("No activities 'due'");
    } else {
        println!("No regimen loaded")
    }
}
