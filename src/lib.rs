#[cfg(feature = "jni")]
use chrono::{DateTime, Duration, Utc};
use log::{debug, trace};
use serde::{Deserialize, Serialize};
use std::fs::{File, OpenOptions};
use std::io::{BufReader, BufWriter};

/// IOS, from https://github.com/debris/rust-react-native-boilerplate/blob/6f9e9fd1149c4e6733c2f7632ed2cac5df471b90/rust/mobile_app/src/lib.rs
extern crate libc;

mod string;

use string::StringPtr;

// string ffi

#[no_mangle]
pub unsafe extern "C" fn rust_string_ptr(s: *mut String) -> *mut StringPtr {
    Box::into_raw(Box::new(StringPtr::from(&**s)))
}

#[no_mangle]
pub unsafe extern "C" fn rust_string_destroy(s: *mut String) {
    let _ = Box::from_raw(s);
}

#[no_mangle]
pub unsafe extern "C" fn rust_string_ptr_destroy(s: *mut StringPtr) {
    let _ = Box::from_raw(s);
}

#[no_mangle]
pub unsafe extern "C" fn ExampleRegimen() -> *mut String {
    let regimen = example_regimen();
    let reg_str = serde_json::to_string(&regimen).unwrap();
    Box::into_raw(Box::new(reg_str))
}

#[no_mangle]
pub unsafe extern "C" fn PushActivity(push_act_command_json: *mut StringPtr) -> *mut String {
    let reg_str = push_act_command((*push_act_command_json).as_str());
    Box::into_raw(Box::new(reg_str))
}

#[no_mangle]
pub unsafe extern "C" fn PopActivity(pop_act_command_json: *mut StringPtr) -> *mut String {
    let ret_str = pop_act_command((*pop_act_command_json).as_str());
    Box::into_raw(Box::new(ret_str))
}

/// Android
extern crate jni;

use self::jni::objects::{JClass, JString};
use self::jni::sys::jstring;
use self::jni::JNIEnv;

pub fn load_regimen(path: &str) -> Option<Regimen> {
    debug!("opening '{}'", path);
    let file = File::open(path).ok()?;
    debug!("reading '{:#?}'", file);
    let reader = BufReader::new(file);
    serde_json::from_reader(reader).ok()
}

pub fn save_regimen(regimen: &Regimen, path: &str) -> Option<()> {
    let file = OpenOptions::new().read(true).write(true).open(path).ok()?;
    let writer = BufWriter::new(file);
    serde_json::to_writer_pretty(writer, &regimen).ok()
}

#[no_mangle]
pub extern "C" fn Java_com_remind_1react_1ts_RemindBridge_exampleRegimen(
    env: JNIEnv,
    _: JClass,
) -> jstring {
    let regimen = example_regimen();
    let reg_str = serde_json::to_string(&regimen).unwrap();
    env.new_string(reg_str).unwrap().into_inner()
}

#[no_mangle]
pub extern "C" fn Java_com_remind_1react_1ts_RemindBridge_pushActivity(
    env: JNIEnv,
    _: JClass,
    push_act_command_json: JString,
) -> jstring {
    let push_act_command_json: String = env.get_string(push_act_command_json).unwrap().into();
    let regimen = push_act_command(&push_act_command_json);
    env.new_string(regimen).unwrap().into_inner()
}

#[no_mangle]
pub extern "C" fn Java_com_remind_1react_1ts_RemindBridge_popActivity(
    env: JNIEnv,
    _: JClass,
    pop_act_command_json: JString,
) -> jstring {
    let pop_act_command_json: String = env.get_string(pop_act_command_json).unwrap().into();
    let pop_act_ret = pop_act_command(&pop_act_command_json);
    env.new_string(pop_act_ret).unwrap().into_inner()
}

fn pop_act_command(pop_act_command_json: &str) -> String {
    let pop_act_cmd: PopActCmd = serde_json::from_str(pop_act_command_json).unwrap();
    let mut regimen = pop_act_cmd.regimen;
    let activity = regimen.pop_activity(Utc::now(), &pop_act_cmd.tags);
    let pop_act_ret = PopActRet { regimen, activity };
    serde_json::to_string_pretty(&pop_act_ret).unwrap()
}

fn push_act_command(push_act_command_json: &str) -> String {
    let push_act_cmd: PushActCmd = serde_json::from_str(push_act_command_json).unwrap();
    let mut regimen = push_act_cmd.regimen;
    regimen.push_activity(push_act_cmd.activity, push_act_cmd.r_value, Utc::now());
    serde_json::to_string_pretty(&regimen).unwrap()
}

#[derive(Debug, Deserialize, Serialize)]
struct PopActRet {
    regimen: Regimen,
    activity: Option<Activity>,
}

#[derive(Debug, Deserialize, Serialize)]
struct PopActCmd {
    regimen: Regimen,
    tags: Vec<String>,
}

#[derive(Debug, Deserialize, Serialize)]
struct PushActCmd {
    regimen: Regimen,
    activity: Activity,
    r_value: f32,
}

fn example_regimen() -> Regimen {
    Regimen {
        activities: vec![
            Activity {
                title: "Practice Fur Elise".to_string(),
                practices: vec![
                    Practice {
                        date: Utc::now() - Duration::seconds(600),
                        duration_ms: 300000,
                        interval_ms: 300000,
                    },
                    Practice {
                        date: Utc::now() - Duration::seconds(300),
                        duration_ms: 400000,
                        interval_ms: 300000,
                    },
                ],
                tags: vec!["piano".to_string(), "music".to_string()],
                ..Default::default()
            },
            Activity {
                title: "hola".to_string(),
                practices: vec![
                    Practice {
                        date: Utc::now() - Duration::seconds(9),
                        duration_ms: 1000,
                        interval_ms: 1000,
                    },
                    Practice {
                        date: Utc::now() - Duration::seconds(5),
                        duration_ms: 1000,
                        interval_ms: 1200,
                    },
                    Practice {
                        date: Utc::now() - Duration::seconds(2),
                        duration_ms: 1000,
                        interval_ms: 1440,
                    },
                ],
                tags: vec!["spanish".to_string(), "language".to_string()],
                value: Some("hello".to_string()),
                ..Default::default()
            },
            Activity {
                title: "cuchillo".to_string(),
                tags: vec!["spanish".to_string(), "language".to_string()],
                value: Some("knife".to_string()),
                ..Default::default()
            },
        ],
    }
}

#[no_mangle]
pub fn print_example() {
    let r = example_regimen();
    let s = serde_json::to_string_pretty(&r).unwrap();
    println!("{}", s);
}

#[derive(Debug, Deserialize, Serialize)]
pub struct Regimen {
    pub activities: Vec<Activity>,
}

impl Default for Regimen {
    fn default() -> Regimen {
        Regimen { activities: vec![] }
    }
}

impl Regimen {
    pub fn push_activity(&mut self, mut activity: Activity, r_value: f32, time_end: DateTime<Utc>) {
        debug!("Pushing activity, start: {:#?}", activity);
        let time_start = if let Some(p) = activity.practices.last() {
            p.date
        } else {
            time_end - Duration::seconds(1)
        };
        let duration_ms = (time_end - time_start).num_milliseconds();
        let average_duration_ms = (activity
            .practices
            .iter()
            .fold(0, |acc, p| acc + p.duration_ms) as f64
            / activity.practices.len() as f64) as i64;
        let interval_ms = std::cmp::max(
            (activity.get_last_interval_ms() as f32 * r_value) as i64,
            average_duration_ms,
        );

        activity.practices.pop();
        let practice = Practice {
            date: time_start,
            duration_ms,
            interval_ms,
            ..Default::default()
        };
        activity.practices.push(practice);
        debug!("Pushing activity, final: {:#?}", activity);
        self.activities.push(activity);
    }

    pub fn pop_activity(&mut self, time: DateTime<Utc>, tags: &[String]) -> Option<Activity> {
        trace!("pop_activity with tags {:?} @ {:?}", tags, time);
        self.activities
            .sort_by_key(|activity| activity.get_due_date_ms());
        // get first activity that is "due"
        let pos = self
            .activities
            .iter()
            .position(|activity| {
                if matching_tag(&activity, &tags) {
                    if let Some(practice) = activity.practices.last() {
                        // check last practice if due
                        practice.date + Duration::milliseconds(practice.interval_ms) <= time
                    } else {
                        false // unpracticed activities
                    }
                } else {
                    false // no matching tags
                }
            })
            .or_else(|| {
                // if nothing ready, fetch an unpractice activity
                self.activities.iter().position(|activity| {
                    if matching_tag(&activity, &tags) {
                        if activity.practices.is_empty() {
                            // check for unpractice items
                            true // unpracticed activities
                        } else {
                            false // practiced activities
                        }
                    } else {
                        false // no matching tags
                    }
                })
            })?;
        trace!("get activity");
        let mut activity = self.activities.get(pos)?.to_owned();
        activity.practices.push(Practice {
            date: time,
            interval_ms: activity.get_last_interval_ms(),
            duration_ms: activity.get_last_duration_ms(),
        });
        self.activities.remove(pos);
        Some(activity)
    }

    pub fn add_activity(
        &mut self,
        new_activity: &str,
        content: Option<String>,
        value: Option<String>,
        tags: &[String],
    ) -> bool {
        if self.activities.iter().any(|a| a.title == new_activity) {
            println!("'{}' already exists, choose a different name", new_activity);
            return false;
        }
        &self.activities.push(Activity {
            title: new_activity.to_string(),
            tags: tags.to_vec(),
            content,
            value,
            ..Default::default()
        });
        return true;
    }
}

fn matching_tag(activity: &Activity, tags: &[String]) -> bool {
    tags.is_empty() || activity.tags.iter().any(|t| tags.contains(t))
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Activity {
    pub title: String,
    pub content: Option<String>,
    pub value: Option<String>,
    pub practices: Vec<Practice>,
    pub tags: Vec<String>,
}

impl Default for Activity {
    fn default() -> Activity {
        Activity {
            title: "activity name".to_string(),
            content: None,
            value: None,
            practices: vec![],
            tags: vec![],
        }
    }
}

impl Activity {
    pub fn get_last_duration_ms(&self) -> i64 {
        if let Some(p) = self.practices.last() {
            std::cmp::max(p.duration_ms, 1000)
        } else {
            1000
        }
    }

    pub fn get_average_duration_ms(&self) -> i64 {
        (self.practices.iter().fold(0, |acc, p| acc + p.duration_ms) as f64
            / self.practices.len() as f64) as i64
    }

    pub fn get_last_interval_ms(&self) -> i64 {
        if let Some(p) = self.practices.last() {
            if p.interval_ms > 1000 {
                p.interval_ms
            } else {
                self.get_last_duration_ms()
            }
        } else {
            1000
        }
    }

    pub fn get_due_date_ms(&self) -> i64 {
        if let Some(p) = self.practices.last() {
            p.interval_ms + p.date.timestamp_millis()
        } else {
            0
        }
    }
    pub fn get_content(&self) -> Option<String> {
        self.content.clone()
    }
    pub fn get_value(&self) -> Option<String> {
        self.value.clone()
    }
}
#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Practice {
    pub date: DateTime<Utc>,
    pub duration_ms: i64,
    pub interval_ms: i64,
}

impl Default for Practice {
    fn default() -> Practice {
        Practice {
            date: Utc::now(),
            duration_ms: 1000,
            interval_ms: 1000,
        }
    }
}

#[cfg(test)]
mod tests {
    use chrono::TimeZone;
    // Note this useful idiom: importing names from outer (for mod tests) scope.
    use super::*;
    fn env_logger_init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    #[test]
    fn test_get_interval() {
        env_logger_init();
        let a = Activity {
            title: "title".to_string(),
            practices: vec![
                Practice {
                    date: Utc.ymd(2020, 11, 15).and_hms(12, 0, 1),
                    duration_ms: 300000,
                    interval_ms: 300000,
                },
                Practice {
                    date: Utc.ymd(2020, 11, 15).and_hms(12, 0, 4),
                    duration_ms: 300000,
                    interval_ms: 320000,
                },
            ],
            ..Default::default()
        };
        assert_eq!(a.get_last_interval_ms(), 320000);
        let a_no_interval = Activity {
            title: "title".to_string(),
            practices: vec![Practice {
                interval_ms: 0,
                date: Utc.ymd(2020, 11, 15).and_hms(12, 0, 1),
                duration_ms: 1300,
            }],
            ..Default::default()
        };
        assert_eq!(a_no_interval.get_last_interval_ms(), 1300);
        let a_no_practice = Activity {
            title: "title".to_string(),
            ..Default::default()
        };
        assert_eq!(a_no_practice.get_last_interval_ms(), 1000);
    }
    #[test]
    fn test_get_activity() {
        env_logger_init();
        let mut r = example_regimen();
        let time_pop = Utc::now();
        let popped_act = r.pop_activity(time_pop, &[]).unwrap();
        assert_eq!(popped_act.title, "hola");
        assert_eq!(popped_act.practices.last().unwrap().date, time_pop);

        // tags
        assert_eq!(
            r.pop_activity(Utc::now(), &["piano".to_string()])
                .unwrap()
                .title,
            "Practice Fur Elise"
        );
    }
    #[test]
    fn test_put_activity() {
        let mut r = Regimen {
            activities: vec![Activity {
                ..Default::default()
            }],
        };
        let time_pop = Utc::now();
        let popped_act = r.pop_activity(time_pop, &[]).unwrap();
        r.push_activity(popped_act, 1.2, time_pop + Duration::seconds(2));
        assert_eq!(
            r.activities.last().unwrap().practices.last().unwrap().date,
            time_pop
        );
    }
    #[test]
    fn test_add_activity() {
        env_logger_init();
        let mut r = Regimen { activities: vec![] };
        assert!(r.pop_activity(Utc::now(), &[]).is_none());
        r.add_activity(
            "yo",
            None,
            Some("hello".to_string()),
            &["slang".to_string(), "english".to_string()],
        );
        r.add_activity(
            "yo",
            None,
            Some("hello".to_string()),
            &["slang".to_string(), "english".to_string()],
        );
        assert_eq!(r.pop_activity(Utc::now(), &[]).unwrap().title, "yo");
        assert!(r.pop_activity(Utc::now(), &[]).is_none());
    }

    #[test]
    fn test_average_duration() {
        let activity = Activity {
            practices: vec![
                Practice {
                    duration_ms: 1000,
                    ..Default::default()
                },
                Practice {
                    duration_ms: 3000,
                    ..Default::default()
                },
                Practice {
                    duration_ms: 3000,
                    ..Default::default()
                },
            ],
            ..Default::default()
        };
        assert_eq!(activity.get_average_duration_ms(), 2333);
    }
}
